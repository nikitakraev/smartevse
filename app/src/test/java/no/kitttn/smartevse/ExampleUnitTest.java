package no.kitttn.smartevse;

import org.junit.Assert;
import org.junit.Test;

import io.realm.Realm;
import no.kitttn.smartevse.di.components.AppComponent;
import no.kitttn.smartevse.di.components.DaggerAppComponent;
import no.kitttn.smartevse.di.modules.AppModule;
import no.kitttn.smartevse.model.User;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {

}
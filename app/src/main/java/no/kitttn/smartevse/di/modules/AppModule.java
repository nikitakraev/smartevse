package no.kitttn.smartevse.di.modules;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author kitttn
 */
@Module
public class AppModule {
	private Context ctx;

	@Provides @Singleton
	public Context provideContext() {
		return ctx;
	}

	public AppModule(Context ctx) {
		this.ctx = ctx;
	}
}

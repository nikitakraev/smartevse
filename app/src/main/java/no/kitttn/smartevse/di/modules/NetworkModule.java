package no.kitttn.smartevse.di.modules;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.kitttn.smartevse.API;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author kitttn
 */
@Module @Singleton
public class NetworkModule {
	@Provides
	public Retrofit provideNetwork(Gson gson) {
		return new Retrofit.Builder()
				.baseUrl("http://10.17.3.236/api/")
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();
	}

	@Provides
	public API provideAPI(Retrofit retrofit) {
		return retrofit.create(API.class);
	}
}
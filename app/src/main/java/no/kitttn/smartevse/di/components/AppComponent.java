package no.kitttn.smartevse.di.components;

import javax.inject.Singleton;

import dagger.Component;
import no.kitttn.smartevse.API;
import no.kitttn.smartevse.App;
import no.kitttn.smartevse.activities.LoginActivity;
import no.kitttn.smartevse.activities.MainActivity;
import no.kitttn.smartevse.di.modules.AppModule;
import no.kitttn.smartevse.di.modules.DBModule;
import no.kitttn.smartevse.di.modules.NetworkModule;
import no.kitttn.smartevse.presenters.LoginPresenter;

/**
 * @author kitttn
 */
@Singleton
@Component(modules = { NetworkModule.class, DBModule.class, AppModule.class })
public interface AppComponent {
	API getAPI();
	void inject(App app);
	void inject(LoginPresenter presenter);
	void inject(MainActivity activity);
}

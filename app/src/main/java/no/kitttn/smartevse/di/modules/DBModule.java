package no.kitttn.smartevse.di.modules;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;

/**
 * @author kitttn
 */
@Module
public class DBModule {
	@Provides @Singleton
	public Realm provideRealm(Context ctx) {
		RealmConfiguration config = new RealmConfiguration.Builder(ctx)
				.deleteRealmIfMigrationNeeded()
				.build();
		return Realm.getInstance(config);
	}

	@Provides @Singleton
	public Gson provideGson() {
		return new GsonBuilder()
				.setExclusionStrategies(new ExclusionStrategy() {
					@Override
					public boolean shouldSkipField(FieldAttributes f) {
						return f.getDeclaringClass().equals(RealmObject.class);
					}

					@Override
					public boolean shouldSkipClass(Class<?> clazz) {
						return false;
					}
				})
				.create();
	}
}

package no.kitttn.smartevse.activities

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import no.kitttn.smartevse.Const
import no.kitttn.smartevse.presenters.LoginPresenter
import no.kitttn.smartevse.views.LoginView
import org.jetbrains.anko.*

/**
 * @author kitttn
 */
class LoginActivity : Activity(), LoginView {
	val presenter = LoginPresenter(this)
	var dialog: ProgressDialog? = null

	override fun loading() {
		dialog = indeterminateProgressDialog("Loading, wait...", "Checking")
	}

	override fun completed() {
		startActivity<MainActivity>()
	}

	override fun error() {
		dialog?.dismiss()
		toast("Please, try again!")
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		verticalLayout {
			val login = editText() {
				topPadding = dip(60)
			}.lparams(width = dip(120))

			val pwd = editText().lparams(width = dip(120))

			button("Log in").onClick {
				presenter.authorize("${login.text}", "${pwd.text}")
			}
		}
	}
}

package no.kitttn.smartevse.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import no.kitttn.smartevse.App;
import no.kitttn.smartevse.R;
import no.kitttn.smartevse.adapters.CardsAdapter;
import no.kitttn.smartevse.model.Car;
import no.kitttn.smartevse.model.User;
import no.kitttn.smartevse.views.MainView;

/**
 * @author kitttn
 */

public class MainActivity extends Activity implements MainView {
	@Inject Realm realm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		App.getGraph().inject(this);

		User activeUser = realm.where(User.class).findFirst();
		List<Car> cars = Arrays.asList(
				new Car("bmw 1970 (darling)", 65, "3h"),
				new Car("Porsche Cayenne 2014", 45, "2h")
		);

		RecyclerView recyclerView = (RecyclerView) findViewById(R.id.cardsLayout);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		CardsAdapter adapter = new CardsAdapter(activeUser, this, cars);
		AlphaInAnimationAdapter animator = new AlphaInAnimationAdapter(adapter);
		animator.setDuration(2000);
		recyclerView.setAdapter(animator);
	}
}

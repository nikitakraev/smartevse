package no.kitttn.smartevse;

import no.kitttn.smartevse.model.User;
import retrofit2.http.POST;
import rx.Observable;

/**
 * @author kitttn
 */
public interface API {
	@POST("users/profile.php")
	Observable<User> getProfile();
}

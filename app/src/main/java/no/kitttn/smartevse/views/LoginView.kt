package no.kitttn.smartevse.views

/**
 * @author kitttn
 */

interface LoginView {
	fun loading();
	fun completed();
	fun error();
}
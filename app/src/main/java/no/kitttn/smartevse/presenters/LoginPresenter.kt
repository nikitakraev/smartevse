package no.kitttn.smartevse.presenters

import io.realm.Realm
import no.kitttn.smartevse.API
import no.kitttn.smartevse.App
import no.kitttn.smartevse.model.User
import no.kitttn.smartevse.views.LoginView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author kitttn
 */
class LoginPresenter {
	private lateinit var loginView: LoginView
	@Inject lateinit var api: API
	@Inject lateinit var realm: Realm

	constructor(loginView: LoginView) {
		this.loginView = loginView
		App.graph.inject(this)
	}

	fun setView(view: LoginView) {
		loginView = view
	}
	fun authorize(login: String, pwd: String) {
		loginView.loading()

		api.profile
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
					{ user: User ->
						realm.executeTransaction { it.copyToRealmOrUpdate(user) };
						loginView.completed()
					},
					{ it.printStackTrace(); loginView.error() }
				)
	}
}

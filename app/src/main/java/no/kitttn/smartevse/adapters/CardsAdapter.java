package no.kitttn.smartevse.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.squareup.picasso.Picasso;

import java.util.List;

import no.kitttn.smartevse.R;
import no.kitttn.smartevse.model.Car;
import no.kitttn.smartevse.model.User;

/**
 * @author kitttn
 */
public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.CardViewHolder> {
	private User user;
	Context context;
	private List<Car> cars;

	public CardsAdapter(User user, Context ctx, List<Car> cars) {
		this.user = user;
		context = ctx;
		this.cars = cars;
	}

	@Override
	public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		int inflationId = R.layout.card_user_profile; // user profile by default

		switch (viewType) {
			case 1: // car card
				inflationId = R.layout.card_car_profile;
				break;
			case 2:
				inflationId = R.layout.card_map;
		}

		View view = LayoutInflater.from(parent.getContext()).inflate(inflationId, parent, false);
		return new CardViewHolder(view, viewType);
	}

	@Override
	public void onBindViewHolder(CardViewHolder holder, int position) {
		if (position == 0) {
			holder.userEmail.setText(user.getEmail());
			holder.textUnderTheImage.setText(user.getName());
			Picasso.with(context).load(user.getImage()).into(holder.profileImage);
		} else if (position == cars.size() + 1) {
			// maps here, nothing to bind

		} else {
			Car car = cars.get(position - 1);
			holder.headerCard.setText(car.getName().toUpperCase());
			int color;

			if (car.getPercentage() > 75 && car.getPercentage() <= 100)
				color = Color.GREEN;
			else if (car.getPercentage() >= 50 && car.getPercentage() <= 75)
				color = Color.rgb(0xff, 0x66, 0);
			else if (car.getPercentage() >= 25 && car.getPercentage() < 80)
				color = Color.YELLOW;
			else color = Color.RED;

			holder.timeLeft.setText("About " + car.getTimeLeft() + " left");

			holder.progress.setColor(color);
			holder.progress.setProgress(car.getPercentage());
			holder.progress.startAnimation();
			holder.textUnderTheImage.setText(car.getPercentage() + "%");
		}
	}

	@Override
	public int getItemCount() {
		return 2 + cars.size();
	}

	@Override
	public int getItemViewType(int position) {
		System.out.println("Position: " + position);
		if (position == 0)
			return 0;
		else if (position == cars.size() + 1)
			return 2;
		return 1;
	}

	class CardViewHolder extends RecyclerView.ViewHolder {
		ImageView profileImage;
		TextView textUnderTheImage;
		CircularProgressView progress;
		TextView headerCard;
		TextView timeLeft;
		TextView userEmail;

		public CardViewHolder(View itemView, int viewType) {
			super(itemView);
			switch (viewType) {
				case 0: // user card
					profileImage = (ImageView) itemView.findViewById(R.id.userProfilePhoto);
					textUnderTheImage = (TextView) itemView.findViewById(R.id.userProfileNameTxt);
					userEmail = (TextView) itemView.findViewById(R.id.userProfileEmailTxt);
					break;

				case 1: // car card
					textUnderTheImage = (TextView) itemView.findViewById(R.id.cardCarEstimatedChargeText);
					progress = (CircularProgressView) itemView.findViewById(R.id.progress_view);
					headerCard = (TextView) itemView.findViewById(R.id.cardCarName);
					timeLeft = (TextView) itemView.findViewById(R.id.timeLeft);
			}
		}
	}
}

package no.kitttn.smartevse

import android.app.Application
import no.kitttn.smartevse.di.components.AppComponent
import no.kitttn.smartevse.di.components.DaggerAppComponent
import no.kitttn.smartevse.di.modules.AppModule

/**
 * @author kitttn
 */
class App : Application() {
	companion object {
		@JvmStatic lateinit var graph: AppComponent
	}

	override fun onCreate() {
		super.onCreate()

		graph = DaggerAppComponent.builder().appModule(AppModule(applicationContext)).build()
		graph.inject(this)
	}
}

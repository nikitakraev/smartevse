package no.kitttn.smartevse.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author kitttn
 */
public class User extends RealmObject {
	@PrimaryKey
	private int id;
	private String login;
	private String email;
	private String name;
	private String image;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}

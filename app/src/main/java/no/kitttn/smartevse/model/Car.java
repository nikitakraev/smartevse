package no.kitttn.smartevse.model;

/**
 * @author kitttn
 */
public class Car {
	public String name;
	public int percentage;
	public String timeLeft;

	public String getTimeLeft() {
		return timeLeft;
	}

	public void setTimeLeft(String timeLeft) {
		this.timeLeft = timeLeft;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public Car(String name, int percentage, String timeLeft) {
		this.name = name;
		this.percentage = percentage;
		this.timeLeft = timeLeft;
	}
}
